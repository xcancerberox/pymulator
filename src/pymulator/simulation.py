"""
El objetivo del simulador es poder armar estas estructuras y poder pedirle los
estados a medida que avanza la simulacion

Integrador con entrada constante
1 -----\int------

Integrador con entrada variable
gen -----\int------

Caso complejo con realimentacion e inyeccion de perturbacion
gen---controlModelo----Modelo---
       ^               |  ^
       |_______________|  |
gen_______________________|
"""


class Simulation():
    def __init__(self, config, timestep=1):
        self.config = config
        self.timestep = timestep
        self.models = {}
        self.data = {}
        self.time = []
        for block_name, block_data in config.items():
            self.models[block_name] = block_data["model"](**block_data["initial_values"])
            self.data[block_name] = {}
            for key in self.models[block_name].output().keys():
                self.data[block_name][key] = []

    def loop(self, steps):
        for i in range(steps):
            # Aca hay que sacar una foto de todas las salidas/entradas
            # para poder simular todos los bloques en cualquier orden
            snapshot = self.models.copy()
            self.time.append(self.timestep*i)

            for model_name, model in self.models.items():
                # Aca hay que intepretar si el input es una variable de otro
                # modelo o una constante
                inputs = {}
                for input_name, asigned_signal in self.config[model_name]["inputs"].items():
                    inputs[input_name] = snapshot[asigned_signal[0]].output()[asigned_signal[1]]

                model.next_state(self.timestep, **inputs)
                for key, value in model.output().items():
                    self.data[model_name][key].append(value)
